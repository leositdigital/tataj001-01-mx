package com.bbva.tata.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.bbva.tata.dto.account.y.AccountDTO;

public class FileItemProcess implements ItemProcessor<AccountDTO,AccountDTO>{
	private static final Logger LOGGER = LoggerFactory.getLogger(FileItemProcess.class);
	
	@Override
	public AccountDTO process(AccountDTO accountIn) throws Exception {
		LOGGER.info("@@@@@ Qué llego al proceso {}",accountIn);
		//aplicar alguna operación sobre los datos recibidos
		
		return accountIn;
	}

}
