package com.bbva.tata.batch;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.bbva.tata.dto.account.y.AccountDTO;
import com.bbva.tata.lib.rj01.TATARJ01;

public class FileItemWriter implements ItemWriter<AccountDTO> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileItemWriter.class);
	
    TATARJ01 tataRJ01;
	public void setTataRJ01(TATARJ01 tataRJ01) {
		this.tataRJ01 = tataRJ01;
	}
    
    @Override
	public void write(List<? extends AccountDTO> listAccounts) throws Exception {
		 for(AccountDTO account : listAccounts) {
			 int result = tataRJ01.executeInsertAccount(account);
		 }		
	}

}
