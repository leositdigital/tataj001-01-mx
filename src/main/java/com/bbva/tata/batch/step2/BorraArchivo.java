package com.bbva.tata.batch.step2;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

public class BorraArchivo implements Tasklet{	
	private Resource resource;
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BorraArchivo.class);
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {		
		
		String filepathStr = resource.getFile().toString();
		LOGGER.info("@@@@ vamos a borrar el archivo {} ",filepathStr);
		Path filePath = Paths.get(filepathStr);
	    Files.delete(filePath);
	    
		return RepeatStatus.FINISHED;
	}

}
