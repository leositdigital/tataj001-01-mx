package com.bbva.tata.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.bbva.tata.dto.account.y.AccountDTO;

public class FileItemReader implements FieldSetMapper<AccountDTO>{
	private static final Logger LOGGER = LoggerFactory.getLogger(FileItemReader.class);
    
	@Override
	public AccountDTO mapFieldSet(FieldSet fileRow) throws BindException {
		
		   AccountDTO accountDTOIn = new AccountDTO();
		   String nuAccount = fileRow.readString("NUMCUENTA");
		   String cdDivisa = fileRow.readString("CDDIVISA");
		   String tpAccount = fileRow.readString("TPCUENTA");
		   String saldo = fileRow.readString("SALDO");
		   accountDTOIn.setNumCuenta(nuAccount);
		   accountDTOIn.setCdDivisa(cdDivisa);
		   accountDTOIn.setTpCuenta(tpAccount);
		   accountDTOIn.setSaldo(new Double(saldo));
		   
		return accountDTOIn;
	}

	
	//	@Override
//	public String mapFieldSet(FieldSet fileRow) throws BindException {
//		   String nuAccount = fileRow.readString("NUMCUENTA");
//		   String cdDivisa = fileRow.readString("CDDIVISA");
//         String tpAccount = fileRow.readString("TPCUENTA");
//         String saldo = fileRow.readString("SALDO");
//         LOGGER.info("@@@@ qué leemos {} ",nuAccount+" "+cdDivisa+ " "+tpAccount+ " "+saldo);
//		return nuAccount;
//	}

}
